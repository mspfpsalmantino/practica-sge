import httplib, urllib, base64
import json

apiKey = "99a2793c760341e48b13acfbed9f497b"

def corregirTexto(text):   
	headers = {
		# Request headers
		'Content-Type': 'application/x-www-form-urlencoded',
		'Ocp-Apim-Subscription-Key': apiKey,
	}

	params = urllib.urlencode({
		# Request parameters
		'mode': 'spell',
	})

	try:
		conn = httplib.HTTPSConnection('api.cognitive.microsoft.com')
		conn.request("POST", "/bing/v5.0/spellcheck/?%s" % params, "text=" + text, headers)
		response = conn.getresponse()
		data = response.read()
		conn.close()

		correccionesJson = json.loads(data)
		flaggedTokens = correccionesJson['flaggedTokens']
		for flaggedToken in flaggedTokens:
			token = flaggedToken["token"]
			suggestion = flaggedToken["suggestions"][0]["suggestion"]
			text = text.replace(token, suggestion)

		return text
	except Exception as e:
		return None