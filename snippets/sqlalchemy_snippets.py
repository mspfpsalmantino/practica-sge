def delete_sqlalchemy_entity(session, entity):
    session.delete(entity)
    session.commit()
    session.close()
    return True

def update_sqlalchemy_entity(session, entity, **fields):
    for x in fields:
        try:
            if hasattr(entity, x):
                setattr(entity, x, fields[x])
        except Exception as e:
            pass
    session.add(entity)
    session.commit()
    return entity

def create_sqlalchemy_entity(session, entity, **fields):
    e = entity()
    for x in fields:
        try:
            if hasattr(e, x):
                setattr(e, x, fields[x])
        except Exception as e:
            pass
    session.add(e)
    session.commit()
    return e